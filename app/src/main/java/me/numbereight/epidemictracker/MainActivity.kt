package me.numbereight.epidemictracker

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import me.numbereight.insights.Insights

class MainActivity : AppCompatActivity() {
    companion object {
        const val LOCATION_REQUEST = 0

        const val COUGH = "cough"
        const val FEVER = "fever"
        const val HEADACHE = "headache"
        const val SHORT_BREATH = "short_breath"
        const val SORE_THROAT = "sore_throat"
    }

    private val symptomByButtonId = mapOf(
        R.id.cough to COUGH,
        R.id.fever to FEVER,
        R.id.headache to HEADACHE,
        R.id.shortBreath to SHORT_BREATH,
        R.id.soreThroat to SORE_THROAT
    )

    private var symptoms: Bundle
    get() {
        val bundle = Bundle()
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        bundle.putBoolean(COUGH, prefs.getBoolean(COUGH, false))
        bundle.putBoolean(FEVER, prefs.getBoolean(FEVER, false))
        bundle.putBoolean(HEADACHE, prefs.getBoolean(HEADACHE, false))
        bundle.putBoolean(SHORT_BREATH, prefs.getBoolean(SHORT_BREATH, false))
        bundle.putBoolean(SORE_THROAT, prefs.getBoolean(SORE_THROAT, false))
        return bundle
    }
    set(value) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        prefs.edit()
            .putBoolean(COUGH, value.getBoolean(COUGH, false))
            .putBoolean(FEVER, value.getBoolean(FEVER, false))
            .putBoolean(HEADACHE, value.getBoolean(HEADACHE, false))
            .putBoolean(SHORT_BREATH, value.getBoolean(SHORT_BREATH, false))
            .putBoolean(SORE_THROAT, value.getBoolean(SORE_THROAT, false))
            .apply()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (BuildConfig.DEBUG) {
            val deviceIdKey = "device_id"
            val prefs = PreferenceManager.getDefaultSharedPreferences(this)
            val id = prefs.getString(deviceIdKey, null)
            findViewById<TextView>(R.id.deviceId).text = id
        }

        setupNotifications()
        setupMonitor()
    }

    override fun onResume() {
        super.onResume()
        setupPowerManagement()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            LOCATION_REQUEST -> {
                setupMonitor()
                return
            }
            else -> {
                // Ignore all other requests.
            }
        }
    }

    private fun setupMonitor() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_REQUEST)
            return
        }

        if (!MonitoringService.IS_SERVICE_RUNNING) {
            val intent = Intent(this, MonitoringService::class.java)
            intent.action = MonitoringService.ACTION.START_FOREGROUND
            startService(intent)
        }
    }

    private fun setupNotifications() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = getSystemService(NotificationManager::class.java)

            val mainChannel = NotificationChannel(
                MonitoringService.NOTIFICATION.CHANNEL_ID,
                getString(R.string.main_notification_channel_name),
                NotificationManager.IMPORTANCE_LOW)
            mainChannel.description = getString(R.string.main_notification_channel_description)

            // Register the channels with the system; you can't change the importance
            // or other notification behaviors after this unless the app is uninstalled
            notificationManager?.createNotificationChannel(mainChannel)
        }
    }

    private fun setupPowerManagement() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        if (!prefs.getBoolean("battery_dialog_shown", false)) {
            val dialog = BatteryOptimizationUtil.getBatteryOptimizationDialog(this, {
                prefs.edit()
                    .putBoolean("battery_dialog_shown", true)
                    .putBoolean("battery_settings_updated", true)
                    .apply()
            }, {
                prefs.edit()
                    .putBoolean("battery_dialog_shown", true)
                    .putBoolean("battery_settings_updated", false)
                    .apply()
            })

            dialog?.show()
        }
    }

    private fun updateButtonStates() {
        val currentSymptoms = symptoms
        for ((id, symptom) in symptomByButtonId.entries) {
            val button = findViewById<ImageButton>(id)
            button.isSelected = currentSymptoms.getBoolean(symptom) == true
        }
    }

    fun toggleSymptom(view: View) {
        val symptom = symptomByButtonId[view.id] ?: return

        val newSymptoms = symptoms
        newSymptoms.putBoolean(symptom, !newSymptoms.getBoolean(symptom, false))
        symptoms = newSymptoms

        updateButtonStates()

        Insights.addMarker("symptoms", newSymptoms)
    }

    @Suppress("UNUSED_PARAMETER")
    fun clearSymptoms(view: View) {
        val newSymptoms = Bundle()
        symptoms = newSymptoms

        updateButtonStates()

        Insights.addMarker("symptoms", newSymptoms)
    }
}
