package me.numbereight.epidemictracker

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.preference.PreferenceManager
import android.util.Log
import androidx.core.app.NotificationCompat
import me.numbereight.insights.Insights
import me.numbereight.insights.RecordingConfig
import me.numbereight.sdk.NumberEight
import java.util.*
import android.os.SystemClock
import android.app.AlarmManager
import android.content.Context
import android.content.Context.ALARM_SERVICE
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T



class MonitoringService : Service() {
    companion object {
        var IS_SERVICE_RUNNING = false
            @Synchronized private set
            @Synchronized get

        private const val LOG_TAG = "ContextMonitorService"
    }

    class ACTION {
        companion object {
            const val START_FOREGROUND = "me.numbereight.epidemictracker.action.startforeground"
            const val STOP_FOREGROUND = "me.numbereight.epidemictracker.action.stopforeground"
            const val RESTART_INTENT = 101
        }
    }

    class NOTIFICATION {
        companion object {
            const val NOTIFICATION_ID = 8
            const val CHANNEL_ID = "me.numbereight.epidemictracker.channel.monitornotification"
        }
    }

    private val deviceId: String
    get() {
        val deviceIdKey = "device_id"
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        var id = prefs.getString(deviceIdKey, null)

        if (id == null) {
            id = UUID.randomUUID().toString()
            prefs.edit().putString(deviceIdKey, id).apply()
        }

        return id
    }

    private val handler: Handler = Handler(Looper.getMainLooper())
    private var token: NumberEight.APIToken? = null

    override fun onCreate() {
        super.onCreate()
        token = NumberEight.start("TOA0J7GBEVTSCK1TMFKG7R9OKLSX", this)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        when (intent?.action) {
            ACTION.START_FOREGROUND -> {
                Log.v(LOG_TAG, "Received START_FOREGROUND intent.")
                start()
            }

            ACTION.STOP_FOREGROUND -> {
                Log.v(LOG_TAG, "Received STOP_FOREGROUND intent.")
                stop()
            }

            else -> {
                Log.e(LOG_TAG, "Unknown intent received: " + intent?.action)
            }
        }

        return START_STICKY
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        // Restart in 5 seconds
        val timeout = SystemClock.elapsedRealtime() + 5000

        val intent = Intent(applicationContext, javaClass)
        val pendingIntent = PendingIntent.getService(this, ACTION.RESTART_INTENT, intent, PendingIntent.FLAG_ONE_SHOT)

        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        alarmManager?.set(AlarmManager.RTC_WAKEUP, timeout, pendingIntent)
        super.onTaskRemoved(rootIntent)
    }

    private fun start() {
        handler.post {
            if (!IS_SERVICE_RUNNING) {
                try {
                    val config = RecordingConfig()
                    config.deviceId = deviceId
                    config.mostProbableOnly = true
                    config.topics = listOf(
                        NumberEight.kNETopicActivity,
                        NumberEight.kNETopicIndoorOutdoor,
                        NumberEight.kNETopicSituation,
                        NumberEight.kNETopicPlace,
                        NumberEight.kNETopicCluster,
                        NumberEight.kNETopicLSTMCluster
                    )
                    config.filters = mapOf(
                        NumberEight.kNETopicActivity to "avg:20s,6s|uniq",
                        NumberEight.kNETopicIndoorOutdoor to "avg:20s,6s|uniq",
                        NumberEight.kNETopicSituation to "avg:30s,6s|uniq",
                        NumberEight.kNETopicPlace to "avg:30s|uniq",
                        NumberEight.kNETopicCluster to "uniq",
                        NumberEight.kNETopicLSTMCluster to "uniq"
                    )

                    val token = token
                    if (token != null) {
                        Insights.startRecording(token, config)
                    }

                    val notification = makeNotification()
                    startForeground(NOTIFICATION.NOTIFICATION_ID, notification)
                    setStatus(true)
                    Log.i(LOG_TAG, "Monitoring service started.")
                } catch (ex: Exception) {
                    Log.e(LOG_TAG, "An exception occurred while starting the monitoring service:", ex)
                }
            }
        }
    }

    private fun stop() {
        handler.post {
            if (IS_SERVICE_RUNNING) {
                Insights.endRecording()
                stopForeground(true)
                setStatus(false)
                Log.i(LOG_TAG, "Monitoring service stopped.")
            }
        }
    }

    private fun makeNotification(): Notification {
        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
        val pendingIntent = PendingIntent.getActivity(this, 0,
            notificationIntent, 0)

        return NotificationCompat.Builder(this, NOTIFICATION.CHANNEL_ID)
            .setTicker("Epidemic Tracker")
            .setContentTitle("Monitoring Outbreak")
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentIntent(pendingIntent)
            .setOngoing(true)
            .build()
    }

    private fun setStatus(isRunning: Boolean) {
        IS_SERVICE_RUNNING = isRunning
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }
}
